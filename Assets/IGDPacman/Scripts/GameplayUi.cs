﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameplayUi : MonoBehaviour {

	public static GameplayUi Instance;

	public Image[] currentLives;
	public Text currentLevel;
	public Text currentTotalScore;

	private string stringTotalScore;

	void Awake ()
	{
		if (!Instance)
			Instance = this;
		else
			Destroy (this.gameObject);
	}

	void Start ()
	{
		currentLevel.text = GameManager.Instance.CurrentLevel.ToString ();
		UpdateScore ();
		UpdateLives ();
	}

	public void UpdateScore ()
	{
		stringTotalScore = GameManager.Instance.totalScore.ToString();
		int zero = 6 - stringTotalScore.Length;

		while (zero > 0)
		{
			stringTotalScore = "0" + stringTotalScore;
			zero--;
		}

		currentTotalScore.text = stringTotalScore;
	}

	public void UpdateLives ()
	{
		int lives = GameManager.Instance.totalLives - 1;

		for (int l = 0; l < currentLives.Length; l++)
		{
			if (lives < l)
				currentLives [l].gameObject.SetActive (false);
			else
				currentLives [l].gameObject.SetActive (true);
		}
	}
}
