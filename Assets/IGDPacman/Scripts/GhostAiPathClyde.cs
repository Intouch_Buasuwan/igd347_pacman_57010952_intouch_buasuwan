﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GhostAiPathClyde : MonoBehaviour {

	public float speed;

	private Vector2 startPosition;
	private Vector2 middleToHuntPosition;
	private Vector2 exitToHuntPosition;
	private Vector2 destination;
	private Map CurrentMap;
	private List<Tile> mPath;
	private PacmanController Pacman;

	private Vector2 tempPosition;
	private Animator chAnimator;

	private float timeInSpawn = 40f;
	private bool isInBox = true;
	private bool isHunt = false;
	private Vector2 middleBoxPosition;

	void OnEnable ()
	{
		GameManager.LevelOpen += GhostStartLevel;
		GameManager.LevelReset += GhostResetLevel;
	}

	void OnDisable ()
	{
		GameManager.LevelOpen -= GhostStartLevel;
		GameManager.LevelReset -= GhostResetLevel;
	}

	void Start ()
	{
		if (GameManager.Instance)
			speed = GameManager.Instance.ghostSpeed;

		GameObject mapTemp = GameObject.FindGameObjectWithTag ("Map");
		CurrentMap = mapTemp.GetComponent <Map> ();

		Pacman = PacmanController.Instance.GetComponent<PacmanController> ();
		chAnimator = GetComponent <Animator> ();

		exitToHuntPosition = new Vector2 (14.5f, 19f);
		middleToHuntPosition = new Vector2 (14.5f, 16f);
		destination = exitToHuntPosition;
		startPosition = transform.position;
		tempPosition = transform.position;

		StartCoroutine (InSpawnBox ());
	}

	void FixedUpdate ()
	{
		if (!Pacman.isPaused)
		{
			float tempX = transform.position.x - tempPosition.x;
			float tempY = transform.position.y - tempPosition.y;

			chAnimator.SetFloat ("DirX", tempX);
			chAnimator.SetFloat ("DirY", tempY);

			tempPosition = transform.position;

			timeInSpawn -= Time.deltaTime;

			if (timeInSpawn <= 0)
			{
				timeInSpawn = float.MaxValue;
				isInBox = false;
				StartCoroutine (OutOfSpawnBox ());
			}

			if (isHunt)
			{
				Vector2 p = Vector2.MoveTowards (transform.position, destination, speed);
				GetComponent <Rigidbody2D> ().MovePosition (p);

				if (Vector2.Distance (destination, transform.position) < 0.00001f)
				{
					int ghostTilePosX = Mathf.FloorToInt (transform.position.x);
					int ghostTilePosY = Mathf.FloorToInt (transform.position.y);
					Tile ghostTile = CurrentMap.GetTileAt (ghostTilePosX, ghostTilePosY);

					if (ghostTile.NeighbourCount >= 2)
					{
						int pacmanTilePosX = Mathf.FloorToInt (Pacman.transform.position.x);
						int pacmanTilePosY = Mathf.FloorToInt (Pacman.transform.position.y);
						Tile pacmanTile = CurrentMap.GetTileAt (pacmanTilePosX, pacmanTilePosY);

						mPath = CurrentMap.FindShortestPath (ghostTile, pacmanTile);
						if (mPath.Count > 1)
							destination = new Vector2 (mPath [1].PosX, mPath [1].PosY);
					}
				}
			}
		}
	}

	IEnumerator InSpawnBox ()
	{
		Vector2 inBoxTop = transform.position;
		Vector2 inBoxBottom = transform.position;

		inBoxTop.y += 0.5f;
		inBoxBottom.y -= 0.5f;


		while (isInBox)
		{
			if (Pacman.isPaused)
				break;
			
			while (Vector2.Distance (inBoxBottom, transform.position) > 0.00001f)
			{
				Vector2 p = Vector2.MoveTowards (transform.position, inBoxBottom, speed);
				GetComponent <Rigidbody2D> ().MovePosition (p);
				yield return null;
			}

			while (Vector2.Distance (inBoxTop, transform.position) > 0.00001f)
			{
				Vector2 p = Vector2.MoveTowards (transform.position, inBoxTop, speed);
				GetComponent <Rigidbody2D> ().MovePosition (p);
				yield return null;
			}
		}

		yield return null;
	}

	IEnumerator OutOfSpawnBox ()
	{
		while (Vector2.Distance (middleToHuntPosition, transform.position) > 0.00001f)
		{
			Vector2 p = Vector2.MoveTowards (transform.position, middleToHuntPosition, speed);
			GetComponent <Rigidbody2D> ().MovePosition (p);
			yield return null;
		}

		while (Vector2.Distance (exitToHuntPosition, transform.position) > 0.00001f)
		{
			Vector2 p = Vector2.MoveTowards (transform.position, exitToHuntPosition, speed);
			GetComponent <Rigidbody2D> ().MovePosition (p);
			yield return null;
		}

		isHunt = true;
		yield return null;
	}

	void OnDrawGizmos ()
	{
		if (mPath == null)
			return;
		if (mPath.Count == 0)
			return;

		Gizmos.color = Color.green;
		for (int i = 0; i < mPath.Count - 1; i++)
			Gizmos.DrawLine (new Vector3 (mPath [i].PosX, mPath [i].PosY, 0),
			                 new Vector3 (mPath [i + 1].PosX, mPath [i + 1].PosY, 0));
	}

	void GhostStartLevel ()
	{
		StartCoroutine (InSpawnBox ());
	}

	public void GhostResetLevel ()
	{
		this.transform.position = startPosition;
		destination = exitToHuntPosition;
		timeInSpawn = 40f;
		isHunt = false;
		isInBox = true;
		StartCoroutine (InSpawnBox ());
	}
}
