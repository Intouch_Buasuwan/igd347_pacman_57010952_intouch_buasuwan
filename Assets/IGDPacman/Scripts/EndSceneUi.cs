﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndSceneUi : MonoBehaviour {

	public Text finalScore;

	private string finalScoreString;

	IEnumerator Start ()
	{		
		finalScoreString = GameManager.Instance.totalScore.ToString ();
		int zero = 6 - finalScoreString.Length;

		while (zero > 0)
		{
			finalScoreString = "0" + finalScoreString;
			zero--;
		}

		finalScore.text = finalScoreString;

		yield return null;

		while (!Input.GetKeyDown (KeyCode.Return))
			yield return null;

		GameManager.Instance.GoToMainMenu ();
	}
}
