﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager Instance;
	public delegate void ResetAction ();
	public static event ResetAction LevelOpen;
	public static event ResetAction LevelReset;

	public string[] AllLevelSceneNames;
	public string OpenSceneName;
	public string EndSceneName;
	public int CurrentLevel;

	public int totalLives;
	public int totalScore;
	public float pacmanSpeed;
	public float ghostSpeed;

	public AudioSource bgMusic;
	public AudioSource levelOpeningMusic;
	public AudioSource pacmanSfx;
	public AudioSource pacmanDeath;

	void Awake ()
	{
		if (!Instance)
			Instance = this;
		else
			Destroy (this.gameObject);

		DontDestroyOnLoad (this.gameObject);
	}

	public void GoToNextLevel ()
	{
		CurrentLevel++;
		string sceneName = "";

		if (CurrentLevel >= AllLevelSceneNames.Length)
			sceneName = AllLevelSceneNames [AllLevelSceneNames.Length - 1];
		else
			sceneName = AllLevelSceneNames [CurrentLevel];

		if (CurrentLevel == 1)
		{
			totalLives = 3;
			totalScore = 0;
		}

		SceneManager.LoadScene (sceneName);
	}

	public void GoToMainMenu ()
	{
		SceneManager.LoadScene (OpenSceneName);
		CurrentLevel = 0;
	}

	public void GoToGameOver ()
	{
		bgMusic.Stop ();
		SceneManager.LoadScene (EndSceneName);
	}

	public void PacmanIsDead ()
	{
		StartCoroutine (PacmanIsDeadSequence ());
	}

	public void LevelOpening ()
	{
		StartCoroutine (LevelOpeningSequence ());
	}

	IEnumerator PacmanIsDeadSequence ()
	{
		PacmanController.Instance.isPaused = true;
		pacmanSfx.Stop ();
		bgMusic.Pause ();

		yield return new WaitForSeconds (1);

		PacmanController.Instance.PacmanDeadAnimation ();
		pacmanDeath.Play ();
		totalLives--;
		GameplayUi.Instance.UpdateLives ();
		yield return new WaitForSeconds (2);

		if (totalLives > 0)
		{
			bgMusic.UnPause ();
			PacmanController.Instance.PacmanResetLevel ();
			if (LevelReset != null)
				LevelReset ();
		}
		else
			GoToGameOver ();			
			
		yield return null;
	}

	IEnumerator LevelOpeningSequence ()
	{
		bgMusic.Pause ();
		levelOpeningMusic.Play ();

		if (pacmanSfx.isPlaying)
			pacmanSfx.Stop ();

		yield return new WaitForSeconds (4.5f);

		pacmanSfx.Play ();
		bgMusic.Play ();
		PacmanController.Instance.isPaused = false;
		if (LevelOpen != null)
			LevelOpen ();

		yield return null;
	}

	void Update ()
	{
	#if UNITY_EDITOR

		if (Input.GetKey (KeyCode.O) &&
		    Input.GetKey (KeyCode.P))
			GoToNextLevel ();
		
	#endif
	}
}
