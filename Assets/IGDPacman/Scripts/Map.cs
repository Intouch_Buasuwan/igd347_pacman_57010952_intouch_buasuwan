﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Map : MonoBehaviour {

	public List<Tile> Tiles = new List<Tile> ();
	public string MapFilename = "";
	public int Width = 28;
	public int Height = 31;

	public GameObject[] Items;
	public bool itemSpawned = false;
	public Vector2 itemPosition;

	void Awake ()
	{
		string mapData = "";
		string filenamewithFullpath = Application.streamingAssetsPath + "/" + MapFilename;

		StreamReader reader = new StreamReader (filenamewithFullpath);
		mapData = reader.ReadToEnd ();
		reader.Close ();

		int y = Height;
		string[] lines = mapData.Split ('\n');

		for (int l = 0; l < lines.Length; l++)
		{
			int x = 1;
			string line = lines [l];
			for (int c = 0; c < line.Length; c++)
			{
				Tile tile = new Tile (x, y);
				if (line [c] == '0')
					tile.Movable = false;
				else if (line [c] == '1')
					tile.Movable = true;
				Tiles.Add (tile);
				x++;
			}
			y--;
		}

		for (int posX = 1; posX <= Width; posX++)
		{
			for (int posY = 1; posY <= Height; posY++)
			{
				Tile tile = GetTileAt (posX, posY);

				Tile leftTile = GetTileAt (posX - 1, posY);
				if (leftTile != null && leftTile.Movable == true)
				{
					tile.Left = leftTile;
					tile.NeighbourCount++;
				}

				Tile rightTile = GetTileAt (posX + 1, posY);
				if (rightTile != null && rightTile.Movable == true)
				{
					tile.Right = rightTile;
					tile.NeighbourCount++;
				}

				Tile upTile = GetTileAt (posX, posY + 1);
				if (upTile != null && upTile.Movable)
				{
					tile.Up = upTile;
					tile.NeighbourCount++;
				}

				Tile downTile = GetTileAt (posX, posY - 1);
				if (downTile != null && downTile.Movable)
				{
					tile.Down = downTile;
					tile.NeighbourCount++;
				}
			}
		}
	}

	IEnumerator Start ()
	{
		while (!itemSpawned)
		{
			yield return new WaitForSeconds (25);
			if (GameManager.Instance.totalLives < 3 && itemSpawned == false)
			{
				int n = Random.Range (0, 100);
				if (n <= 25)
				{
					int i = Random.Range (0, Items.Length);
					Instantiate (Items [i], itemPosition, Quaternion.identity);
					itemSpawned = true;
				}
			}
		}
	}

	public Tile GetTileAt (int x, int y)
	{
		for (int i = 0; i < Tiles.Count; i++)
		{
			if (Tiles [i].PosX == x && Tiles [i].PosY == y)
				return Tiles [i];
		}
		return null;
	}

	public List<Tile> FindShortestPath (Tile fromTile, Tile toTile)
	{
		for (int x = 1; x <= Width; x++)
		{
			for (int y = 1; y <= Height; y++)
			{
				Tile tile = GetTileAt (x, y);
				tile.TraversalPoint = int.MaxValue;
			}
		}

		Stack<Tile> tileStack = new Stack<Tile> ();
		fromTile.TraversalPoint = 0;
		tileStack.Push (fromTile);

		while (tileStack.Count > 0)
		{
			Tile tile = tileStack.Pop ();

			int v = tile.TraversalPoint + 1;

			if (tile.Up != null)
			{
				if (tile.Up.TraversalPoint > v)
				{
					tile.Up.TraversalPoint = v;
					if (tile.Up != toTile)
						tileStack.Push (tile.Up);
				}
			}

			if (tile.Right != null)
			{
				if (tile.Right.TraversalPoint > v)
				{
					tile.Right.TraversalPoint = v;
					if (tile.Right != toTile)
						tileStack.Push (tile.Right);
				}
			}

			if (tile.Down != null)
			{
				if (tile.Down.TraversalPoint > v)
				{
					tile.Down.TraversalPoint = v;
					if (tile.Down != toTile)
						tileStack.Push (tile.Down);
				}
			}

			if (tile.Left != null)
			{
				if (tile.Left.TraversalPoint > v)
				{
					tile.Left.TraversalPoint = v;
					if (tile.Left != toTile)
						tileStack.Push (tile.Left);
				}
			}
		}

		List<Tile> path = new List<Tile> ();
		Tile traverseBackTile = toTile;
		path.Add (traverseBackTile);

		while (traverseBackTile != fromTile)
		{
			Tile[] neighbourTiles = new Tile[] {
				traverseBackTile.Up,
				traverseBackTile.Right,
				traverseBackTile.Down,
				traverseBackTile.Left
			};

			Tile minTraversePointTile = null;
			int minTraversePoint = int.MaxValue;
			for (int i = 0; i < neighbourTiles.Length; i++)
			{
				if (neighbourTiles [i] == null)
					continue;

				if (neighbourTiles [i].TraversalPoint < minTraversePoint)
				{
					minTraversePoint = neighbourTiles [i].TraversalPoint;
					minTraversePointTile = neighbourTiles [i];
				}
			}

			traverseBackTile = minTraversePointTile;
			path.Add (traverseBackTile);
		}

		path.Reverse ();
		return path;
	}

	void OnDrawGizmos()
	{
		if (Tiles.Count == 0)
			return;

		Gizmos.color = new Color (1, 1, 1, 0.25f);
		for (int posX = 1; posX <= Width; posX++)
		{
			for (int posY = 1; posY <= Height; posY++)
			{
				Tile tile = GetTileAt (posX, posY);
				if (tile.Movable == false)
					continue;

				if (tile.Left != null)
				{
					Vector3 p1 = new Vector3 (tile.PosX, tile.PosY, 0);
					Vector3 p2 = new Vector3 (tile.Left.PosX, tile.Left.PosY, 0);
					Gizmos.DrawLine (p1, p2);
				}
				if (tile.Right != null)
				{
					Vector3 p1 = new Vector3 (tile.PosX, tile.PosY, 0);
					Vector3 p2 = new Vector3 (tile.Right.PosX, tile.Right.PosY, 0);
					Gizmos.DrawLine (p1, p2);
				}
				if (tile.Up != null)
				{
					Vector3 p1 = new Vector3 (tile.PosX, tile.PosY, 0);
					Vector3 p2 = new Vector3 (tile.Up.PosX, tile.Up.PosY, 0);
					Gizmos.DrawLine (p1, p2);
				}
				if (tile.Down != null)
				{
					Vector3 p1 = new Vector3 (tile.PosX, tile.PosY, 0);
					Vector3 p2 = new Vector3 (tile.Down.PosX, tile.Down.PosY, 0);
					Gizmos.DrawLine (p1, p2);
				}
			}
		}
	}
}
