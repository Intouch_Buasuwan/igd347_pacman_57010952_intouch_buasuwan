﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OpenSceneUi : MonoBehaviour {

	public Animator TitleAnimator;
	public Animator DescriptionAnimator;

	IEnumerator Start()
	{
		while (!Input.GetKeyDown (KeyCode.Return))
			yield return null;

		GameManager.Instance.bgMusic.Play ();
		DescriptionAnimator.Play ("Description_fade");

		yield return new WaitForSeconds (1f);

		GameManager.Instance.GoToNextLevel ();
	}
}
