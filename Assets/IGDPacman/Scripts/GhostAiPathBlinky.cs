﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GhostAiPathBlinky : MonoBehaviour {

	public static GhostAiPathBlinky Instance;

	public float speed;

	private Vector2 startPosition;
	private Vector2 destination;
	private Map CurrentMap;
	private List<Tile> mPath;
	private PacmanController Pacman;

	private Vector2 tempPosition;
	private Animator chAnimator;

	void OnEnable ()
	{
		GameManager.LevelReset += GhostResetLevel;
	}

	void OnDisable ()
	{
		GameManager.LevelReset -= GhostResetLevel;
	}

	void Awake ()
	{
		if (!Instance)
			Instance = this;
		else
			Destroy (this.gameObject);

		destination = Vector2.zero;
	}

	void Start()
	{
		if (GameManager.Instance)
			speed = GameManager.Instance.ghostSpeed;

		GameObject mapTemp = GameObject.FindGameObjectWithTag ("Map");
		CurrentMap = mapTemp.GetComponent <Map> ();
		
		Pacman = PacmanController.Instance.GetComponent<PacmanController> ();
		chAnimator = GetComponent <Animator> ();

		destination = transform.position;
		startPosition = transform.position;
		tempPosition = transform.position;
	}
		
	void FixedUpdate ()
	{
		if (!Pacman.isPaused)
		{
			Vector2 p = Vector2.MoveTowards (transform.position, destination, speed);
			GetComponent<Rigidbody2D> ().MovePosition (p);

			float tempX = transform.position.x - tempPosition.x;
			float tempY = transform.position.y - tempPosition.y;

			chAnimator.SetFloat ("DirX", tempX);
			chAnimator.SetFloat ("DirY", tempY);

			tempPosition = transform.position;

			if (Vector2.Distance (destination, transform.position) < 0.00001f)
			{
				int ghostTilePosX = Mathf.FloorToInt (transform.position.x);
				int ghostTilePosY = Mathf.FloorToInt (transform.position.y);
				Tile ghostTile = CurrentMap.GetTileAt (ghostTilePosX, ghostTilePosY);

				if (ghostTile.NeighbourCount >= 2)
				{
					int pacmanTilePosX = Mathf.FloorToInt (Pacman.transform.position.x);
					int pacmanTilePosY = Mathf.FloorToInt (Pacman.transform.position.y);
					Tile pacmanTile = CurrentMap.GetTileAt (pacmanTilePosX, pacmanTilePosY);

					mPath = CurrentMap.FindShortestPath (ghostTile, pacmanTile);
					if (mPath.Count > 1)
						destination = new Vector2 (mPath [1].PosX, mPath [1].PosY);
				}
			}
		}
	}

	void OnDrawGizmos ()
	{
		if (mPath == null)
			return;

		if (mPath.Count == 0)
			return;

		Gizmos.color = Color.green;
		for (int i = 0; i < mPath.Count - 1; i++)
			Gizmos.DrawLine (new Vector3 (mPath [i].PosX, mPath [i].PosY, 0),
			                 new Vector3 (mPath [i + 1].PosX, mPath [i + 1].PosY, 0));
	}

	public void GhostResetLevel ()
	{
		this.transform.position = startPosition;
		destination = transform.position;
	}
}
