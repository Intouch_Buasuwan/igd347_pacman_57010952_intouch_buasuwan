﻿using UnityEngine;
using System.Collections;

public class PacmanController : MonoBehaviour {

	public static PacmanController Instance;

	public float speed;
	public int NumberOfCollectedPacDot = 0;
	public int NumberOfTotalPacDot = 0;
	public bool isPaused;

	public Vector2 startPosition;
	private Vector2 destination;
	private Vector2 direction;
	private Vector2 nextDirection;
	private Animator chAnimator;

	void Awake ()
	{
		if (!Instance)
			Instance = this;
		else
			Destroy (this.gameObject);

		destination = Vector2.zero;
		direction = Vector2.zero;
		nextDirection = Vector2.zero;
		chAnimator = GetComponent<Animator> ();
	}

	void Start ()
	{
		if (GameManager.Instance)
		{
			speed = GameManager.Instance.pacmanSpeed;
			GameManager.Instance.LevelOpening ();
		}
		
		direction = Vector2.right;
		destination = transform.position;
		nextDirection = direction;
		isPaused = true;

		GameObject[] allPacdots = GameObject.FindGameObjectsWithTag ("PacDot");
		NumberOfTotalPacDot = allPacdots.Length;
	}

	void FixedUpdate ()
	{
		if (!isPaused)
		{
			Vector2 p = Vector2.MoveTowards (transform.position, destination, speed);
			GetComponent<Rigidbody2D> ().MovePosition (p);

			if (Input.GetAxis ("Horizontal") > 0)
			{
				nextDirection = Vector2.right;
			}
			else if (Input.GetAxis ("Horizontal") < 0)
			{
				nextDirection = Vector2.left;
			}
			else if (Input.GetAxis ("Vertical") > 0)
			{
				nextDirection = Vector2.up;
			}
			else if (Input.GetAxis ("Vertical") < 0)
			{
				nextDirection = Vector2.down;
			}

			if (Vector2.Distance (destination, transform.position) < 0.00001f)
			{
				if (Valid (nextDirection))
				{
					destination = (Vector2)transform.position + nextDirection;
					direction = nextDirection;
					chAnimator.SetFloat ("DirX", direction.x);
					chAnimator.SetFloat ("DirY", direction.y);
				}
				else 
				{
					if (Valid (direction))
						destination = (Vector2)transform.position + direction;
				}
			}
		}
	}

	bool Valid (Vector2 direction)
	{
		Vector2 pos = transform.position;
		direction += new Vector2 (direction.x * 0.45f, direction.y * 0.45f);
		RaycastHit2D hit = Physics2D.Linecast (pos + direction, pos);

		if (hit.collider == null)
		{
			return true;
		}
		else
		{
			if (hit.collider.transform == this.transform ||
			    hit.transform.CompareTag ("PacDot") ||
			    hit.transform.CompareTag ("PacEnergizer"))
				return true;
			else
				return false;
		}
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("Ghost") == true)
		{
			GameManager.Instance.PacmanIsDead ();
		}

		if (other.gameObject.CompareTag ("PacDot") == true)
		{
			other.gameObject.SetActive (false);
			NumberOfCollectedPacDot++;
			GameManager.Instance.totalScore += 10;
			GameplayUi.Instance.UpdateScore ();

			if (NumberOfTotalPacDot - NumberOfCollectedPacDot <= 10)
				GhostAiPathBlinky.Instance.speed = GameManager.Instance.ghostSpeed + 0.05f;
			else if (NumberOfTotalPacDot - NumberOfCollectedPacDot <= 20)
				GhostAiPathBlinky.Instance.speed = GameManager.Instance.ghostSpeed + 0.025f;

			if (NumberOfCollectedPacDot >= NumberOfTotalPacDot)
				GameManager.Instance.GoToNextLevel ();
		}

		if (other.gameObject.CompareTag ("PacEnergizer") == true)
		{
			other.gameObject.SetActive (false);
			GameManager.Instance.totalScore += 100;
			GameplayUi.Instance.UpdateScore ();
		}

		if (other.gameObject.CompareTag ("Item") == true)
		{
			other.gameObject.SetActive (false);
			GameManager.Instance.totalLives++;
			GameplayUi.Instance.UpdateLives ();
		}
	}

	public void PacmanDeadAnimation ()
	{
		chAnimator.SetBool ("isDead", true);
	}

	public void PacmanResetLevel ()
	{
		this.transform.position = startPosition;
		direction = Vector2.right;
		destination = transform.position;
		nextDirection = direction;
		isPaused = false;
		GameManager.Instance.pacmanSfx.Play ();
		chAnimator.SetBool ("isDead", false);
	}
}
